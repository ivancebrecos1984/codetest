package com.codetest.ivan.master;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.codetest.ivan.R;
import com.codetest.ivan.detail.DetailActivity;
import com.codetest.ivan.detail.DetailFragment;
import com.codetest.ivan.model.TrackModel;

public class ListActivity extends AppCompatActivity implements ListFragment.FragmentListener {

    private boolean isTablet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        ((Toolbar) findViewById(R.id.toolbar)).setTitle(getTitle());

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.list_containerFL, new ListFragment())
                .commit();

        if (findViewById(R.id.detail_containerFL) != null) {
            isTablet = true;
        }
    }

    @Override
    public void trackSelected(TrackModel track) {
        if(isTablet){
            DetailFragment fragmentItem = DetailFragment.newInstance(track);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.detail_containerFL, fragmentItem);
            ft.commit();
        }
        else {
            Intent intent = new Intent(this, DetailActivity.class);
            intent.putExtra(DetailFragment.ID_TRACK, track);

            startActivity(intent);
        }
    }
}