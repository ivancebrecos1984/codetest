package com.codetest.ivan.master;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.codetest.ivan.R;
import com.codetest.ivan.model.TrackModel;
import com.codetest.ivan.model.TrackModelList;
import com.codetest.ivan.requests.ApiUtils;
import com.codetest.ivan.requests.TrackService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ivan on 15/7/17.
 */

public class ListFragment extends Fragment
        implements TrackAdapter.OnItemClickListener {

    private EditText mSearchTermET;
    private Button mSearchBT;
    private RecyclerView mRecyclerView;
    private TrackService mService;
    private TrackAdapter.OnItemClickListener mOnItemClick;

    private FragmentListener fragmentListener;

    public ListFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_list, container, false);

        mSearchTermET = (EditText) v.findViewById(R.id.search_termET);
        mSearchBT = (Button) v.findViewById(R.id.searchBT);
        mRecyclerView = (RecyclerView) v.findViewById(R.id.tracksRV);
        mOnItemClick = this;

        mSearchBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String text = mSearchTermET.getText().toString();
                String criteria = text.replace(' ','+');
                Log.d("","");
                mService = ApiUtils.getTracks();
                getTracks(criteria);
            }
        });

        return v;
    }

    public void getTracks(String criteria) {
        mService.getTracks(criteria).enqueue(new Callback<TrackModelList>() {
            @Override
            public void onResponse(Call<TrackModelList> call, Response<TrackModelList> response) {

                if(response.isSuccessful()) {
                    mRecyclerView.setAdapter(new TrackAdapter(response.body().getResults(), mOnItemClick));
                }else {
                    int statusCode  = response.code();
                    // handle request errors depending on status code
                }
            }

            @Override
            public void onFailure(Call<TrackModelList> call, Throwable t) {
                Log.d("ListFragment", "error loading from API");

            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentListener) {
            fragmentListener = (FragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " should implement FragmentListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        fragmentListener = null;
    }

    public void loadDetail(TrackModel track) {
        if (fragmentListener != null) {
            fragmentListener.trackSelected(track);
        }
    }

    @Override
    public void onClick(TrackModel track) {
        loadDetail(track);
    }

    public interface FragmentListener {
        void trackSelected(TrackModel track);
    }
}