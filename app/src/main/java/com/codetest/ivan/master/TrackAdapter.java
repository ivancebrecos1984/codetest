package com.codetest.ivan.master;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.codetest.ivan.R;
import com.codetest.ivan.model.TrackModel;

import java.util.ArrayList;

/**
 * Created by ivan on 15/7/17.
 */

public class TrackAdapter extends RecyclerView.Adapter<TrackAdapter.ViewHolder> {

    private final ArrayList<TrackModel> tracksList;

    public TrackAdapter(ArrayList<TrackModel> items,
                              OnItemClickListener onItemClick) {
        tracksList = items;
        this.onItemClick = onItemClick;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_tracks, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.item = tracksList.get(position);
        holder.artistNameTV.setText(tracksList.get(position).getArtistName());
        holder.trackNameTV.setText(tracksList.get(position).getTrackName());
        Glide.with(holder.itemView.getContext())
                .load(holder.item.getArtworkUrl100())
                .thumbnail(0.1f)
                .centerCrop()
                .into(holder.artworkIV);

    }

    @Override
    public int getItemCount() {
        if (tracksList != null) {
            return tracksList.size() > 0 ? tracksList.size() : 0;
        } else {
            return 0;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {
        public final TextView artistNameTV;
        public final TextView trackNameTV;
        public final ImageView artworkIV;

        public TrackModel item;

        public ViewHolder(View view) {
            super(view);
            view.setClickable(true);
            artistNameTV = (TextView) view.findViewById(R.id.artist_nameTV);
            trackNameTV = (TextView) view.findViewById(R.id.track_nameTV);
            artworkIV = (ImageView) view.findViewById(R.id.artworkIV);

            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onItemClick.onClick(tracksList.get(getAdapterPosition()));
        }
    }


    public interface OnItemClickListener {
        void onClick(TrackModel track);
    }

    private OnItemClickListener onItemClick;
}