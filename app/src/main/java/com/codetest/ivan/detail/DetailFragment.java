package com.codetest.ivan.detail;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.codetest.ivan.R;
import com.codetest.ivan.model.TrackModel;


/**
 * Created by ivan on 15/7/17.
 */

public class DetailFragment extends Fragment {
    public static final String ID_TRACK = "idTrack";

    private TrackModel mItem;

    public DetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ID_TRACK)) {
            mItem = (TrackModel) getArguments().getSerializable(ID_TRACK);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_detail, container, false);

        if (mItem != null) {

            ((TextView) rootView.findViewById(R.id.track_nameTV)).setText(mItem.getTrackName());
            ((TextView) rootView.findViewById(R.id.album_nameTV)).setText(mItem.getCollectionName());
            ((TextView) rootView.findViewById(R.id.artist_nameTV)).setText(mItem.getArtistName());
            ((TextView) rootView.findViewById(R.id.priceTV)).setText(mItem.getTrackPrice()+ mItem.getCurrency());
            ((TextView) rootView.findViewById(R.id.dateTV)).setText(mItem.getReleaseDate());
            final ImageView image = (ImageView) rootView.findViewById(R.id.artworkIV);

            Glide.with(getContext()).load(mItem.getArtworkUrl100()).asBitmap().centerCrop().into(new BitmapImageViewTarget(image) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable =
                            RoundedBitmapDrawableFactory.create(getContext().getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    image.setImageDrawable(circularBitmapDrawable);
                }
            });
        }

        return rootView;
    }

    public static DetailFragment newInstance(TrackModel trackModel) {
        DetailFragment fragmentDemo = new DetailFragment();
        Bundle args = new Bundle();
        args.putSerializable(DetailFragment.ID_TRACK, trackModel);
        fragmentDemo.setArguments(args);
        return fragmentDemo;
    }
}