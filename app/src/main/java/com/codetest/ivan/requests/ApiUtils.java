package com.codetest.ivan.requests;

/**
 * Created by ivan on 15/7/17.
 */

public class ApiUtils {

    public static final String BASE_URL = "https://itunes.apple.com/";

    public static TrackService getTracks() {
        return RetrofitClient.getClient(BASE_URL).create(TrackService.class);
    }
}
