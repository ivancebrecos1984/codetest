package com.codetest.ivan.requests;

import com.codetest.ivan.model.TrackModelList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by ivan on 15/7/17.
 */

public interface TrackService {
    @GET("search?")
    Call<TrackModelList> getTracks(@Query("term") String criteria);
}
