package com.codetest.ivan.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by ivan on 15/7/17.
 */

public class TrackModelList implements Serializable {

    private int resultCount;
    private ArrayList<TrackModel> results;

    public int getResultCount() {
        return resultCount;
    }

    public void setResultCount(int resultCount) {
        this.resultCount = resultCount;
    }

    public ArrayList<TrackModel> getResults() {
        return results;
    }

    public void setResults(ArrayList<TrackModel> results) {
        this.results = results;
    }
}
